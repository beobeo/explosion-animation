package com.explosion;

import android.graphics.Canvas;
import android.graphics.Rect;


/**
 * The Explosion class.
 */
public class Explosion {
	/** An array of particles for the explosion. */
	private Particle[] particles;
	/** Boolean value for whether an explosion is still alive. */
	private boolean isAlive;
	
	/**
	 * Explosion constructor.
	 * @param particleNr number of particles
	 * @param xCoord x-coordinate
	 * @param yCoord y-coordinate
	 */
	public Explosion(final int particleNr, final int xCoord, final int yCoord) {
		isAlive = true;
		particles = new Particle[particleNr];
	 	for (int i = 0; i < particles.length; i++) {
			Particle p = new Particle(xCoord, yCoord);
			particles[i] = p;
		}
	}
	
	/**
	 * Update method.
	 */
	public final void update() {
		if (isAlive) {
			boolean isDead = true;
			for (int i = 0; i < particles.length; i++) {
				if (particles[i].isAlive()) {
					particles[i].updateTransperancy();
					isDead = false;
				}
			}
			if (isDead) {
				isAlive = false;
			} 
		}
	}
	
	/**
	 * Update.
	 *
	 * @param container the container
	 */
	public final void update(final Rect container) {
		if (isAlive) {
			boolean isDead = true;
			for (int i = 0; i < particles.length; i++) {
				if (particles[i].isAlive()) {
					particles[i].update(container);
					isDead = false;
				}
			}
			if (isDead) {
				isAlive = false;
			} 
		}
	}

	/**
	 * Draws the canvas  with all the particles on it.
	 *
	 * @param canvas the canvas
	 */
	public final void draw(final Canvas canvas) {
		for (int i = 0; i < particles.length; i++) {
			if (particles[i].isAlive()) {
				particles[i].draw(canvas);
			}
		}
	}
	
	
	/**
	 * Gets the state of the explosion.
	 *
	 * @return the state of the explosion
	 */
	public final boolean getState() {
		return isAlive;
	}

}
