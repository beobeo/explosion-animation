package com.explosion;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * The Main Activity Class.
 *
 */
public class MainActivity extends Activity {
	private static final String TAG = "EXPLOSION";
    /**
     * Called when Activity is first created.
     * @param savedInstanceState the state saved bundle
     */
	public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			onRestoreInstanceState(savedInstanceState);
		}
        setContentView(new View(this));
	}
	
	@Override
	protected void onRestart() {
		Log.i(TAG, "onRestart");
		super.onRestart();
	}

	@Override
	protected void onStart() {
		Log.i(TAG, "onStart");
		super.onStart();
	}


	@Override
	protected void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "onStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "onDestroy");
		super.onDestroy();
	}
}