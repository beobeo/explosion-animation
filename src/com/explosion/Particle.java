package com.explosion;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * The particle class.
 *
 */
public class Particle {
	
	/** The maximum size and height of a particle. */
	public static final int MAX_SIZE = 8;
	/** The maximum velocity of a particle. */
	public static final int MAX_SPEED = 10;
	/** The default life span of a particle. */
	public static final int DEFAULT_LIFE = 70;	
	/** The size of a particle. */
	private float partSize;	
	/** The dead or alive state of a particle. */
	private boolean state;	
	/** The horizontal and vertical position of a particle. */
	private float x, y;	
	/** The horizontal and vertical velocity of a particle. */
	private double xv, yv;	
	/** The current age of a particle. */
	private int age;	
	/** The total life span of a particle. It dies when
	 * it reaches value. */
	private int lifespan;	
	/** The color of a particle. */
	private int color;	
	/** An instance of paint to avoid instantiation. */
	private Paint paint;
	
	
	/**
	 * Gets the state of a particle.
	 *
	 * @return the state of a particle
	 */
	public final boolean isAlive() {
		return state;
	}

	/**
	 * Sets the state of a particle.
	 *
	 * @param theState the state of a particle
	 */
	public final void setState(final boolean theState) {
		state = theState;
	}

	/**
	 * Sets the color of a particle.
	 *
	 * @param theColor the new color of a particle
	 */
	public final void setColor(final int theColor) {
		color = theColor;
	}
	
	/**
	 * Instantiates a new particle.
	 *
	 * @param initX the initial x coordinate of a particle
	 * @param initY the initial y coordinate of a particle
	 */
	public Particle(final int initX, final int initY) {
		x = initX;
		y = initY;
		setState(true);
		partSize = randInt(1, MAX_SIZE);
		lifespan = DEFAULT_LIFE;
		age = 0;
		xv = (randDouble(0, MAX_SPEED * 2) - MAX_SPEED);
		yv = (randDouble(0, MAX_SPEED * 2) - MAX_SPEED);
		if (xv * xv + yv * yv > MAX_SPEED * MAX_SPEED) {
			//CHECKSTYLE: OFF
			xv *= 0.5; //horizontal acceleration
			yv *= 0.5; //vertical acceleration
			//CHECKSTYLE: ON
		}
		color = Color.RED;
		paint = new Paint(color);
	}

	/**
	 * Random integer generator.
	 *
	 * @param min the min
	 * @param max the max
	 * @return the random integer
	 */
	static int randInt(final int min, final int max) {
		return (int) (min + Math.random() * (max - min + 1));
	}

	/**
	 * Random double generator.
	 *
	 * @param min the min
	 * @param max the max
	 * @return the random double
	 */
	static double randDouble(final double min, final double max) {
		return min + (max - min) * Math.random();
	}
	
	/**
	 * Updates the particle's transparency and size.
	 */
	public final void updateTransperancy() {
		if (isAlive()) {
			x += xv;
			y += yv;
			//CHECKSTYLE: OFF
			int a = color >>> 24;
			a -= 8;
			if (a <= 0) {
				setState(false);
			} else {
				paint.setAlpha(a);
				age++;
				partSize -= 0.05;
				//CHECKSTYLE: ON
			}
			if (age >= lifespan) {
				setState(false);
			}			
		}
	}
	
	/**
	 * Update method with collision detection for walls.
	 *
	 * @param container the container
	 */
	public final void update(final Rect container) {

		/*if (isAlive()) {
			//side walls collision detection
			if (x <= container.left	|| x >= container.right - partSize) {
				xv *= -1;
			}
			//top and bottom collision detection
			if (y <= container.top || y >= container.bottom - partSize) {
				yv *= -1;
			}
		}*/

		updateTransperancy();
	}

	/**
	 * The draw method.
	 *
	 * @param canvas the canvas
	 */
	public final void draw(final Canvas canvas) {
		paint.setColor(color);
		canvas.drawRect(x, y, x + partSize,
				y + partSize, paint);
	}

}
