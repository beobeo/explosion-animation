package com.explosion;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This is the View. It draws the image to the screen and notifies the 
 * Controller of onTouch events.
 */
public class View extends SurfaceView implements
SurfaceHolder.Callback {

	/** An instance of the Controller. */
	private Controller controller;
	/** An instance of an Explosion. */
	private Explosion explosion;

	/**
	 * The View constructor.
	 * @param context provides information about application environment
	 */
	public View(final Context context) {
		super(context);
		getHolder().addCallback(this);
		setFocusable(true);
	    
	}


	/**
	 * Gets the explosion object.
	 * @return the explosion
	 */
	public final Explosion getExplosion() {
		return explosion;
	}

	/**
	 * Sets the explosion.
	 * @param e the explosion
	 */
	public final void setExplosion(final Explosion e) {
		this.explosion = e;
	}


	/* (non-Javadoc)
	 * @see android.view.SurfaceHolder.
	 * Callback#surfaceChanged(android.view.SurfaceHolder, int, int, int)
	 */
	@Override
	public void surfaceChanged(final SurfaceHolder holder, final int format, 
			final int width, final int height) {
	}

	/* (non-Javadoc)
	 * @see android.view.SurfaceHolder.
	 * Callback#surfaceCreated(android.view.SurfaceHolder)
	 */
	@Override
	public final void surfaceCreated(final SurfaceHolder holder) {
		controller = new Controller(getHolder(), this);
		controller.setRunning(true);
		controller.start();
	}

	/* (non-Javadoc)
	 * @see android.view.SurfaceHolder.
	 * Callback#surfaceDestroyed(android.view.SurfaceHolder)
	 */
	@Override
	public final void surfaceDestroyed(final SurfaceHolder holder) {
		// tell the thread to shut down and wait for it to finish
		// this is a clean shutdown
		boolean retry = true;
		while (retry) {
			try {
				controller.setRunning(false);
				controller.join();
				retry = false;
			} catch (InterruptedException e) {
				controller.setRunning(false);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public final boolean onTouchEvent(final MotionEvent event) {
		controller.touchEvent(event);
		return true;
	}

	/**
	 * Renders the Canvas.
	 * @param canvas the canvas
	 */
	public final void render(final Canvas canvas) {

		canvas.drawColor(Color.WHITE);
		if (explosion != null) {
			explosion.draw(canvas);
		}

		Paint paint = new Paint();
		paint.setColor(Color.BLUE);
		canvas.drawLines(new float[]{
				0, 0, canvas.getWidth() - 1, 0,
				canvas.getWidth() - 1, 0, canvas.getWidth() - 1,
				canvas.getHeight() - 1, canvas.getWidth() - 1,
				canvas.getHeight() - 1, 0, canvas.getHeight() - 1,
				0, canvas.getHeight() - 1, 0, 0
		}, paint);
	}

	/**
	 * This is the game update method. It updates explosions.
	 */
	public final void update() {
		if (explosion != null && explosion.getState()) {
			explosion.update(getHolder().getSurfaceFrame());
		}
	}

}
