/**
 * A package-info for the explosion animation proof of
 * concept program.
 * 
 * @author Robert Kardjaliev
 *
 */
package com.explosion;