package com.explosion;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;


/**
 * The Controller Class contains the game loop. It must have access to 
 * the View and Holder to listen to events and respond appropriately.
 */
public class Controller extends Thread {

	/** Size of the explosion. */
	private static final int EXPLOSION_SIZE = 500;
	/** The surface holder. **/
	private SurfaceHolder surfaceHolder;
	/** The View. **/
	private View view;

	/** Boolean game state. **/
	private boolean gameState;

	/** Setter for game state. 
	* @param running the boolean game state variable 
	**/
	public final void setRunning(final boolean running) {
		gameState = running;
	}

	/**
	 * Constructor for the Thread Class.
	 * @param surface the surface holder
	 * @param theView the View
	 */
	public Controller(final SurfaceHolder surface, final View theView) {
		super();
		surfaceHolder = surface;
		view = theView;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public final void run() {
		Canvas canvas;
		//updates and renders the view
		while (gameState) {
			canvas = null;
			try {
				canvas = this.surfaceHolder.lockCanvas();
				synchronized (surfaceHolder) {
					view.update();
					view.render(canvas);					
				}
			} finally {
				// in case of an exception the surface is not left in 
				// an inconsistent state
				if (canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	/**
	 * Creates an explosion in the event of the screen being touched.
	 * @param event the touch event
	 * @return the explosion
	 */
	public final boolean touchEvent(final MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (view.getExplosion() == null || !view.getExplosion()
					.getState()) {
				view.setExplosion(new Explosion(EXPLOSION_SIZE, 
						(int) event.getX(), (int) event.getY()));
			}
		}
		return true;
		
	}

}
